# README #

Kaplan Test Prep Assignment

### What is this repository for? ###

*Demonstrate the creation of Orders and OrdetItems at once, manage Inventory

### How do I get set up? ###

* After login in Salesforce.com, we have the Kaplan custom application, inside this app, we can see 2 custom tabs, the first related to Inventory.
- The Inventory Tab contains information of the Stock available by product (it means every product has their own inventory).
- Each inventory record contains how much entries (plus quantity of the product) has, and also how many Deliveries has been placed (coming from order Items)

The Structure is:

![Screen Shot 2017-01-31 at 6.45.12 PM.png](https://bitbucket.org/repo/MjjLR9/images/866465677-Screen%20Shot%202017-01-31%20at%206.45.12%20PM.png)


### Massive Upload ###

*The Massive Upload it's a tool that allows users to read a CSV file and create many different orders by customer, basically this tools allow users to set in the CSV file as many orders and items are needed per customer (the tool accept different customers and orders) it means the information is grouped by Customer->Order->Item

This tool is available in the Upload orders tab, and after been selected the order files (Included in the resources folder of this repo) you should see the information previous to be created like this:

![Screen Shot 2017-01-31 at 6.50.17 PM.png](https://bitbucket.org/repo/MjjLR9/images/3206317839-Screen%20Shot%202017-01-31%20at%206.50.17%20PM.png)

When the Generate orders button is clicked the Orders and ItemOrders should be created (we are not considering deep validation per record or inventory at this point).
Also still an issue creating the Order Item that can be consulted here: http://salesforce.stackexchange.com/questions/158386/insert-order-and-orderitem-lists-at-once-in-apex)

The idea is before generate orderItems the inventory should be validated and stop the process if the stock is not enough to cover the total quantity.

Here's the example for the inventory Screen:

![Screen Shot 2017-01-31 at 10.05.02 PM.png](https://bitbucket.org/repo/MjjLR9/images/968047127-Screen%20Shot%202017-01-31%20at%2010.05.02%20PM.png)

Development coverage:
![Screen Shot 2017-01-31 at 10.12.30 PM.png](https://bitbucket.org/repo/MjjLR9/images/1772689959-Screen%20Shot%202017-01-31%20at%2010.12.30%20PM.png)