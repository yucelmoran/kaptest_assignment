public class FacadeController {
	
	/**
	 * [getCustomerList get Accounts included in a Set of Account Numbers]
	 * @param  accountNumbers [description]
	 * @return                [description]
	 */
	public Map<String,Account> getCustomerList(Set<String> accountNumbers){
		Map<String,Account> accountInformation = new Map<String,Account>();
		for(Account account:[Select Id,Name,AccountNumber from Account where AccountNumber IN:accountNumbers]){
			accountInformation.put(account.AccountNumber,account);
		}
		return accountInformation;
	}

	/**
	 * [getProductList Get Products from a set of ProductCodes]
	 * @param  productCodes [description]
	 * @return              [description]
	 */
	public Map<String,Product2> getProductList(Set<String> productCodes){
		Map<String,Product2> productMap = new Map<String,Product2>();
		for(Product2 product:[Select Id,Name,ProductCode from Product2 where ProductCode IN:productCodes]){
			productMap.put(product.ProductCode,product);
		}

		return productMap;
	}

	/**
	 * [getPriceBookEntry Return a map with PriceBook Entry from productIds]
	 * @param  productCodes [description]
	 * @return              [description]
	 */
	public Map<String,PriceBookEntry> getPriceBookEntry(Set<Id> productIds){
		Map<String,PriceBookEntry> mapPBE = new Map<String,PriceBookEntry>();
		
		for(PriceBookEntry pbe:[SELECT Id,Name ,Product2Id FROM PriceBookEntry WHERE Product2Id IN : productIds]){
			mapPBE.put(pbe.Product2Id,pbe);
		}
		
		return mapPBE;
	}

	public Map<String,Order> getCreatedOrders(Set<String> ordersId){
		Map<String,Order> mapOrders=  new Map<String,Order> ();
		for(Order order:[Select Id,Name from Order where Id IN:ordersId]){
			mapOrders.put(order.Name,order);
		}

		return mapOrders;
	}

	public PriceBook2 getActivePriceBook(){
		PriceBook2 priceBook =[SELECT Id,isactive, Name from PriceBook2 where Name ='Standard' limit 1];
		return priceBook;
	}
}