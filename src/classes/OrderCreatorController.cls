public with sharing class OrderCreatorController {

	public String[] csvFileLines{get;set;}
	public String csvAsString{get;set;}
	public Blob csvFileBody{get;set;}
	public Set<String> setLinetimes = new Set<String>(); //to get PBE

	public List<CSVOrder.CustomOrder> customerOrdersDisplay = new List<CSVOrder.CustomOrder>();

	public List<CSVOrder.CustomOrder> getCustomerOrdersDisplay(){
		return this.customerOrdersDisplay;
	}

	public void setCustomerOrdersDisplay(List<CSVOrder.CustomOrder> customOrder){
		this.customerOrdersDisplay = customOrder;
	}

	public OrderCreatorController(){
		csvFileLines = new String[]{};
	}

	public void importCSVFile(){
		csvAsString  = Utilities.blobToString(csvFileBody,'ISO-8859-1');
		csvFileLines = csvAsString.split('\n');
		List<CSVOrder.CustomOrder> orders = convertMapToWrapper(groupInformationFromCSVlines(csvFileLines));
		verifyData(orders);
	}

	private void verifyData(List<CSVOrder.CustomOrder> orders){

		Set<String> customerNumbers = new Set<String>();
		Set<String> productCodes = new Set<String>();
		
		//collect the SFDC information we need to validate the orders can be created.
		for(CSVOrder.CustomOrder customerOderRecord:orders){
			customerNumbers.add(customerOderRecord.customer);
			for(CSVOrder.OrderLine orderLineRecord:customerOderRecord.orders){
				for(CSVOrder.OrderProductLine lineItem:orderLineRecord.productsInOrder){
					productCodes.add(lineItem.productCode);
				}
			}
		}
		//Check the information vs SFDC
		FacadeController facade			= new FacadeController();
		Map<String,Account> accountMap	= facade.getCustomerList(customerNumbers);
		Map<String,Product2> productMap	= facade.getProductList(productCodes);		

		for(CSVOrder.CustomOrder customerOderRecord:orders){
			if(accountMap.get(customerOderRecord.customer)!=null){	
				customerOderRecord.sfdcAccountName	= accountMap.get(customerOderRecord.customer).Name;	
				customerOderRecord.sfdcAccountId	= accountMap.get(customerOderRecord.customer).Id;		
			}
			else{
				customerOderRecord.sfdcAccountName = customerOderRecord.customer+' - CUSTOMER DOES NOT EXIST.' ;	
			}

			for(CSVOrder.OrderLine orderLineRecord:customerOderRecord.orders){			
				for(CSVOrder.OrderProductLine lineItem:orderLineRecord.productsInOrder){
					lineItem.masterOrder = orderLineRecord.orderNumber;

					if(productMap.get(lineItem.productCode)!=null){
						lineItem.productName = productMap.get(lineItem.productCode).Name;
						lineItem.sfdcProductId = productMap.get(lineItem.productCode).Id;
						setLinetimes.add(lineItem.productCode);
					}
					else{
						lineItem.productName = lineItem.productCode + ' - PRODUCT DOES NOT EXIST.';
					}
				}				
			}
			customerOrdersDisplay.add(customerOderRecord);
		}
	}	


	public PageReference save(){
		FacadeController facade	= new FacadeController();
		PriceBook2 pb			= facade.getActivePriceBook();


		Map<String,Product2> productList = facade.getProductList(setLinetimes);
		Set<Id> setId = new Set<Id>();

		for(Product2 product:productList.values()){
			setId.add(product.Id);
		}

		Map<String,PriceBookEntry> priceBookEntryMap= facade.getPriceBookEntry(setId);
		//Map<String,Order> mapOrdersCreated	= facade.getCreatedOrders(setOrdersCreated);
		
		List<Order> ordersToCreate=  new List<Order>();
		List<OrderItem> orderItemList = new List<OrderItem>();
		for(CSVOrder.CustomOrder customerOderRecord:customerOrdersDisplay){
			
			for(CSVOrder.OrderLine orderLineRecord:customerOderRecord.orders){	

				Order orderRecordReferencez			= new Order(ExternalId__c = orderLineRecord.orderNumber);
				Order orderRecord			= new Order();	
				orderRecord.AccountId		= customerOderRecord.sfdcAccountId;
				orderRecord.Description		= orderLineRecord.orderNumber;	
				orderRecord.Status			= 'Draft';
				orderRecord.EffectiveDate	= Date.today();	
				orderRecord.Name			= orderLineRecord.orderNumber;
				orderRecord.PriceBook2Id 	= pb.Id;
				orderRecord.ExternalId__c 	= orderLineRecord.orderNumber;
				ordersToCreate.add(orderRecord);

				for(CSVOrder.OrderProductLine lineItem:orderLineRecord.productsInOrder){					
					if(priceBookEntryMap.get(lineItem.sfdcProductId)!=null){
						OrderItem itemRecord = new OrderItem();
						itemRecord.quantity = lineItem.quantity;
						itemRecord.PriceBookEntryId = priceBookEntryMap.get(lineItem.sfdcProductId).Id;
						itemRecord.Order = orderRecordReferencez;
						orderItemList.add(itemRecord);
					}					
				}
			}
		}
		List<SObject> records = new List<SObject>();
		
		for(Order o:ordersToCreate){
			records.add(o);
		}

		for(OrderItem item:orderItemList){
			records.add(item);
		}

		Database.SaveResult[] results = Database.insert(records,false);
		
        // Check results.
        for (Integer i = 0; i < results.size(); i++) {
            if (results[i].isSuccess()) {
				System.debug('Successfully created ID: '+ results[i].getId());
            } 
            else {
            	System.debug('Error: could not create sobject ' + 'for array element ' + i + '.');
            	System.debug('The error reported was: '+ results[i].getErrors()[0].getMessage() + '\n');
            }
        }

		return null;
	}

	/**
	 * [convertMapToWrapper  Convert the grouped map to an Wrapper Object [CSV Order POJO]]
	 * @param  groupedInformation [description]
	 * @return                    [description]
	 */
	private List<CSVOrder.CustomOrder> convertMapToWrapper(Map<String,Map<String,List<String[]>>> groupedInformation){
		List<CSVOrder.CustomOrder> orderArray = new List<CSVOrder.CustomOrder>();

		for(String customerKey:groupedInformation.keySet()){
			if(groupedInformation.get(customerKey)!=null){
				CSVOrder.CustomOrder customerLevel	= new CSVOrder.CustomOrder();
				customerLevel.customer		= customerKey;

				List<CSVOrder.OrderLine> orderLineArray = new List<CSVOrder.OrderLine>();
				for(String orderKey:groupedInformation.get(customerKey).keySet()){
					CSVOrder.OrderLine order		= new CSVOrder.OrderLine();
					order.orderNumber	= orderKey;
					orderLineArray.add(order);
					
					Map<String,List<String[]>> mapa = groupedInformation.get(customerKey);
					List<CSVOrder.OrderProductLine> lineItems = new List<CSVOrder.OrderProductLine>();
					for(List<String> products:mapa.get(orderKey)){
						CSVOrder.OrderProductLine lineItem	= new CSVOrder.OrderProductLine();
						lineItem.productCode		= products[2];
						lineItem.quantity			= Integer.valueOf(products[3]);
						lineItem.unitPrice			= Double.valueOf(products[4]);
						lineItems.add(lineItem);
					}
					order.productsInOrder = lineItems;				
				}
				customerLevel.orders = orderLineArray;
				orderArray.add(customerLevel);
			}
		}
		return orderArray;
	}

	/**
	 * [groupInformationFromCSVlines Return a grouped Map Customer->Orders->ProductLineItems ]
	 * @param  groupedInformation [description]
	 * @return                    [description]
	 */
	private Map<String,Map<String,List<String[]>>> groupInformationFromCSVlines(String[] csvFileLines){
		Map<String,Map<String,List<String[]>>> generalMap = new Map<String,Map<String,List<String[]>>>();
		for(Integer i=1;i<csvFileLines.size();i++){
			String[] csvRecordData	= csvFileLines[i].split(',');			
			if(generalMap.containsKey(csvRecordData[0])){
				Map<String,List<String[]>> orderMap = generalMap.get(csvRecordData[0]);
				if(orderMap.containsKey(csvRecordData[1])){
					List<String[]> productsArray = orderMap.get(csvRecordData[1]);
					productsArray.add(csvRecordData);
					orderMap.put(csvRecordData[1],productsArray);
				}
				else{
					orderMap.put(csvRecordData[1],new List<String[]>{csvRecordData});
				}
				generalMap.put(csvRecordData[0],orderMap);				
			}
			else{
				Map<String,List<String[]>> orderMaptemp = new Map<String,List<String[]>>();
				if(orderMaptemp.containsKey(csvRecordData[1])){
					List<String[]> productsArray = orderMaptemp.get(csvRecordData[1]);
					productsArray.add(csvRecordData);
					orderMaptemp.put(csvRecordData[1],productsArray);
				}
				else{
					orderMaptemp.put(csvRecordData[1],new List<String[]>{csvRecordData});
				}
				generalMap.put(csvRecordData[0],orderMaptemp);
			}
		}
		return generalMap;
	}
}