/**
 * POJO definition for the CSV order information
 */
public class CSVOrder {
	public class CustomOrder{
    	public String customer{get;set;}
        public String sfdcAccountName{get;set;}
        public String sfdcAccountId{get;set;}
        public String customerStatus{get;set;}
    	public List<OrderLine> orders{get;set;}
    }

    public class OrderLine{
    	public String orderNumber{get;set;}
        public String orderStatus{get;set;}
    	public List<OrderProductLine> productsInOrder{get;set;}
    }

    public class OrderProductLine{
        public String masterOrder{get;set;}
    	public String productCode{get;set;}
        public String productName{get;set;}
        public Id sfdcProductId{get;set;}
    	public Integer quantity{get;set;}
    	public Double unitPrice{get;set;}
    }
}