public class Utilities {
	
	/**
	 * [blobToString Convert a Blob file to String]
	 * @param  input     [description]
	 * @param  inCharset [description]
	 * @return           [description]
	 */
    public static String blobToString(Blob input, String inCharset){
		String hex					= EncodingUtil.convertToHex(input);
		final Integer bytesCount	= hex.length() >> 1;
		String[] bytes				= new String[bytesCount];
       
        for(Integer i = 0; i < bytesCount; ++i){
			bytes[i] =  hex.mid(i << 1, 2);
        }
        return EncodingUtil.urlDecode('%' + String.join(bytes, '%'), inCharset);
    }
}