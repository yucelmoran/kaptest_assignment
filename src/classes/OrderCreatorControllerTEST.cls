@isTest()
private class OrderCreatorControllerTEST {
	
	@isTest static void test_method_one() {
    
		OrderCreatorController controller = new OrderCreatorController();
		List<CSVOrder.CustomOrder> customerorders = controller.getCustomerOrdersDisplay();
		controller.setCustomerOrdersDisplay(customerorders);
    	//preparing information in String
	    String csv_template = '855740,88990,GC1040,2,100.50\n855740,88991,GC1040,2,100.50\n55741,88993,56789,2,100.50';
	    Blob blob_csv = Blob.valueOf(csv_template);
	    controller.csvFileBody = blob_csv;
	    controller.importCSVFile();
	    controller.save();
	}

	@testSetup static void methodName() {
		Account account = new Account();
		account.Name = 'test';
		account.AccountNumber = '855740';
		insert account;

		Account account2 = new Account();
		account2.Name = 'test2';
		account2.AccountNumber = 'r55741';
		insert account2;

		// First, set up test price book entries.
        // Insert a test product.
        Product2 prod = new Product2(Name = 'Laptop X200', 
            Family = 'Hardware',ProductCode='GC1040');
        insert prod;
        
        // Get standard price book ID.
        // This is available irrespective of the state of SeeAllData.
        Id pricebookId = Test.getStandardPricebookId();
        
        // 1. Insert a price book entry for the standard price book.
        // Standard price book entries require the standard price book ID we got earlier.
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        // Create a custom price book
        Pricebook2 customPB = new Pricebook2(Name='Standard', isActive=true);
        insert customPB;
        
        // 2. Insert a price book entry with a custom price.
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;


	}
	
}